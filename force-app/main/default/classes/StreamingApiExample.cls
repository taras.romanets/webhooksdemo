global class StreamingApiExample {

    private static final String CHANNEL = '/topic/AccountUpdates';

    @AuraEnabled
    public static String subscribeToChannel() {
        StreamingSubscription s = new StreamingSubscription();
        s.Name = 'AccountUpdatesSubscription';
        s.Query = 'SELECT Id, Name, Industry FROM Account';
        s.PushTopic = 'AccountUpdates';
        insert s;

        StreamingChannel sc = [SELECT Id, Name FROM StreamingChannel WHERE Name = :CHANNEL LIMIT 1];
        StreamingChannelMember member = new StreamingChannelMember(StreamingChannelId = sc.Id);
        insert member;

        return s.Id;
    }

    @AuraEnabled
    public static void unsubscribeFromChannel(String subscriptionId) {
        StreamingSubscription s = [SELECT Id FROM StreamingSubscription WHERE Id = :subscriptionId LIMIT 1];
        delete s;
    }
}

//* execite anonymously
// PushTopic pushTopic = new PushTopic();
// pushTopic.Name = 'AccountUpdates';
// pushTopic.Query = 'SELECT Id, Name, Industry FROM Account';
// pushTopic.ApiVersion = 52.0;
// insert pushTopic;
