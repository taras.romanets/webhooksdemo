global with sharing class SoapExample {
    webservice static Account getRecord(String accountId) {
        Account result = [SELECT Id, Name, Industry FROM Account WHERE Id = :accountId LIMIT 1];
        return result;
    }
}