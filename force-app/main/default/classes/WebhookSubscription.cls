@RestResource(urlMapping='/api/webhooks/commitDetails/*')
global with sharing class WebhookSubscription {
    @HttpPost
    global static void handleNotification() {

        try {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;

            String hashedval = request.headers.get('X-Hub-Signature-256');
            System.debug('hashedval: ' + hashedval);
            Blob bB = request.requestBody;
            System.debug(bB.toString());

            JSONParser parser = JSON.createParser(request.requestBody.toString());
            List<Commit_Details__c> commitDetailsList = new List<Commit_Details__c>();

            while(parser.nextToken() != null) {

                String userName;
                String userUsername;

                if(parser.getText() == 'user_name') {
                    parser.nextToken();
                    userName = String.valueOf(parser.readValueAs(String.Class));
                }

                if(parser.getText() == 'user_username') {
                    parser.nextToken();
                    userUsername = String.valueOf(parser.readValueAs(String.Class));
                }

                if(parser.getText() == 'commits') {
                    parser.nextToken();
                    List<CommitWrapper> commits = (List<CommitWrapper>)parser.readValueAs(List<CommitWrapper>.Class);

                    for(CommitWrapper c : commits) {
                        commitDetailsList.add(new Commit_Details__c(
                            Name = c.id,
                            Commit_Message__c = c.message,
                            Commit_Title__c = c.title,
                            Commit_URL__c = c.url,
                            Added_Components__c = JSON.serialize(c.added),
                            Removed_Components__c = JSON.serialize(c.removed),
                            Modified_Components__c = JSON.serialize(c.modified),
                            Commiter_Name__c = userName,
                            User_Username__c = userUsername
                        ));
                    }
                }
            }

            insert commitDetailsList;

        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }
    }

    public class CommitWrapper {
        public String id;
        public String message;
        public String title;
        public String url;
        public List<String> added;
        public List<String> removed;
        public List<String> modified;

        public CommitWrapper(String id, String message, String title, String url, List<String> added, List<String> removed, List<String> modified) {
            this.id = id;
            this.message = message;
            this.title = title;
            this.url = url;
            this.added = added;
            this.removed = removed;
            this.modified = modified;
        }
    }
}